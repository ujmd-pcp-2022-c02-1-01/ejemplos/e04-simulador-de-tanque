﻿namespace Simulador_de_Tanque_de_Cervecería
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Principal_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panelInteriorTanque = new System.Windows.Forms.Panel();
            this.panelCerveza = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnLlenar = new System.Windows.Forms.Button();
            this.btnVaciar = new System.Windows.Forms.Button();
            this.btnParo = new System.Windows.Forms.Button();
            this.panelLlenando = new System.Windows.Forms.Panel();
            this.PanelVaciando = new System.Windows.Forms.Panel();
            this.tmrLlenado = new System.Windows.Forms.Timer(this.components);
            this.tmrVaciado = new System.Windows.Forms.Timer(this.components);
            this.tmrEstado = new System.Windows.Forms.Timer(this.components);
            this.panelLleno = new System.Windows.Forms.Panel();
            this.panelVacío = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.nudAlturaTanque = new System.Windows.Forms.NumericUpDown();
            this.nudDiámetro = new System.Windows.Forms.NumericUpDown();
            this.lblNivelCerveza = new System.Windows.Forms.Label();
            this.lblVolumen = new System.Windows.Forms.Label();
            this.lblLitros = new System.Windows.Forms.Label();
            this.Principal_tableLayoutPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelInteriorTanque.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAlturaTanque)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDiámetro)).BeginInit();
            this.SuspendLayout();
            // 
            // Principal_tableLayoutPanel
            // 
            this.Principal_tableLayoutPanel.ColumnCount = 2;
            this.Principal_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 248F));
            this.Principal_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Principal_tableLayoutPanel.Controls.Add(this.panel1, 1, 0);
            this.Principal_tableLayoutPanel.Controls.Add(this.panel4, 0, 0);
            this.Principal_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Principal_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.Principal_tableLayoutPanel.Name = "Principal_tableLayoutPanel";
            this.Principal_tableLayoutPanel.RowCount = 1;
            this.Principal_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Principal_tableLayoutPanel.Size = new System.Drawing.Size(557, 355);
            this.Principal_tableLayoutPanel.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(251, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(303, 349);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.panel2.BackgroundImage = global::Simulador_de_Tanque_de_Cervecería.Properties.Resources.Recurso_3480;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel2.Controls.Add(this.panelInteriorTanque);
            this.panel2.Controls.Add(this.panelVacío);
            this.panel2.Controls.Add(this.panelLleno);
            this.panel2.Location = new System.Drawing.Point(13, 9);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(170, 331);
            this.panel2.TabIndex = 1;
            // 
            // panelInteriorTanque
            // 
            this.panelInteriorTanque.BackColor = System.Drawing.Color.Black;
            this.panelInteriorTanque.Controls.Add(this.panelCerveza);
            this.panelInteriorTanque.Location = new System.Drawing.Point(101, 104);
            this.panelInteriorTanque.Name = "panelInteriorTanque";
            this.panelInteriorTanque.Size = new System.Drawing.Size(13, 127);
            this.panelInteriorTanque.TabIndex = 2;
            // 
            // panelCerveza
            // 
            this.panelCerveza.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(159)))), ((int)(((byte)(49)))));
            this.panelCerveza.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelCerveza.Location = new System.Drawing.Point(0, 60);
            this.panelCerveza.Name = "panelCerveza";
            this.panelCerveza.Size = new System.Drawing.Size(13, 67);
            this.panelCerveza.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.tableLayoutPanel1);
            this.panel4.Controls.Add(this.tableLayoutPanel2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(242, 349);
            this.panel4.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.Controls.Add(this.btnLlenar, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnVaciar, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnParo, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.panelLlenando, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.PanelVaciando, 1, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(13, 15);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(153, 135);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // btnLlenar
            // 
            this.btnLlenar.Location = new System.Drawing.Point(3, 3);
            this.btnLlenar.Name = "btnLlenar";
            this.btnLlenar.Size = new System.Drawing.Size(117, 29);
            this.btnLlenar.TabIndex = 0;
            this.btnLlenar.Text = "Llenar";
            this.btnLlenar.UseVisualStyleBackColor = true;
            this.btnLlenar.Click += new System.EventHandler(this.btnLlenar_Click);
            // 
            // btnVaciar
            // 
            this.btnVaciar.Location = new System.Drawing.Point(3, 38);
            this.btnVaciar.Name = "btnVaciar";
            this.btnVaciar.Size = new System.Drawing.Size(117, 29);
            this.btnVaciar.TabIndex = 0;
            this.btnVaciar.Text = "Vaciar";
            this.btnVaciar.UseVisualStyleBackColor = true;
            this.btnVaciar.Click += new System.EventHandler(this.btnVaciar_Click);
            // 
            // btnParo
            // 
            this.btnParo.Location = new System.Drawing.Point(3, 73);
            this.btnParo.Name = "btnParo";
            this.btnParo.Size = new System.Drawing.Size(117, 29);
            this.btnParo.TabIndex = 0;
            this.btnParo.Text = "Paro";
            this.btnParo.UseVisualStyleBackColor = true;
            this.btnParo.Click += new System.EventHandler(this.btnParo_Click);
            // 
            // panelLlenando
            // 
            this.panelLlenando.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(32)))), ((int)(((byte)(0)))));
            this.panelLlenando.Location = new System.Drawing.Point(127, 4);
            this.panelLlenando.Margin = new System.Windows.Forms.Padding(4);
            this.panelLlenando.Name = "panelLlenando";
            this.panelLlenando.Size = new System.Drawing.Size(22, 27);
            this.panelLlenando.TabIndex = 1;
            // 
            // PanelVaciando
            // 
            this.PanelVaciando.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(32)))), ((int)(((byte)(0)))));
            this.PanelVaciando.Location = new System.Drawing.Point(127, 39);
            this.PanelVaciando.Margin = new System.Windows.Forms.Padding(4);
            this.PanelVaciando.Name = "PanelVaciando";
            this.PanelVaciando.Size = new System.Drawing.Size(22, 27);
            this.PanelVaciando.TabIndex = 1;
            // 
            // tmrLlenado
            // 
            this.tmrLlenado.Interval = 20;
            this.tmrLlenado.Tick += new System.EventHandler(this.tmrLlenado_Tick);
            // 
            // tmrVaciado
            // 
            this.tmrVaciado.Interval = 20;
            this.tmrVaciado.Tick += new System.EventHandler(this.tmrVaciado_Tick);
            // 
            // tmrEstado
            // 
            this.tmrEstado.Enabled = true;
            this.tmrEstado.Tick += new System.EventHandler(this.tmrEstado_Tick);
            // 
            // panelLleno
            // 
            this.panelLleno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panelLleno.Location = new System.Drawing.Point(71, 104);
            this.panelLleno.Margin = new System.Windows.Forms.Padding(4);
            this.panelLleno.Name = "panelLleno";
            this.panelLleno.Size = new System.Drawing.Size(22, 10);
            this.panelLleno.TabIndex = 1;
            // 
            // panelVacío
            // 
            this.panelVacío.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panelVacío.Location = new System.Drawing.Point(72, 221);
            this.panelVacío.Margin = new System.Windows.Forms.Padding(4);
            this.panelVacío.Name = "panelVacío";
            this.panelVacío.Size = new System.Drawing.Size(22, 10);
            this.panelVacío.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 83F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.nudAlturaTanque, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.nudDiámetro, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblNivelCerveza, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblVolumen, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblLitros, 1, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(13, 173);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(203, 167);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 35);
            this.label1.TabIndex = 0;
            this.label1.Text = "Altura Tanque (m):";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 35);
            this.label2.TabIndex = 0;
            this.label2.Text = "Diámetro (m):";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 35);
            this.label3.TabIndex = 0;
            this.label3.Text = "Nivel de cerveza (m):";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(3, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 32);
            this.label4.TabIndex = 0;
            this.label4.Text = "Volumen (m³):";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(3, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 30);
            this.label5.TabIndex = 0;
            this.label5.Text = "Volumen (l):";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // nudAlturaTanque
            // 
            this.nudAlturaTanque.DecimalPlaces = 2;
            this.nudAlturaTanque.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudAlturaTanque.Location = new System.Drawing.Point(123, 3);
            this.nudAlturaTanque.Name = "nudAlturaTanque";
            this.nudAlturaTanque.Size = new System.Drawing.Size(77, 20);
            this.nudAlturaTanque.TabIndex = 1;
            this.nudAlturaTanque.Value = new decimal(new int[] {
            28,
            0,
            0,
            65536});
            // 
            // nudDiámetro
            // 
            this.nudDiámetro.DecimalPlaces = 2;
            this.nudDiámetro.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudDiámetro.Location = new System.Drawing.Point(123, 73);
            this.nudDiámetro.Name = "nudDiámetro";
            this.nudDiámetro.Size = new System.Drawing.Size(77, 20);
            this.nudDiámetro.TabIndex = 1;
            this.nudDiámetro.Value = new decimal(new int[] {
            9,
            0,
            0,
            65536});
            // 
            // lblNivelCerveza
            // 
            this.lblNivelCerveza.Location = new System.Drawing.Point(123, 35);
            this.lblNivelCerveza.Name = "lblNivelCerveza";
            this.lblNivelCerveza.Size = new System.Drawing.Size(77, 32);
            this.lblNivelCerveza.TabIndex = 0;
            this.lblNivelCerveza.Text = "0.00";
            this.lblNivelCerveza.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblVolumen
            // 
            this.lblVolumen.Location = new System.Drawing.Point(123, 105);
            this.lblVolumen.Name = "lblVolumen";
            this.lblVolumen.Size = new System.Drawing.Size(77, 32);
            this.lblVolumen.TabIndex = 0;
            this.lblVolumen.Text = "0.00";
            this.lblVolumen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLitros
            // 
            this.lblLitros.Location = new System.Drawing.Point(123, 137);
            this.lblLitros.Name = "lblLitros";
            this.lblLitros.Size = new System.Drawing.Size(77, 30);
            this.lblLitros.TabIndex = 0;
            this.lblLitros.Text = "0.00";
            this.lblLitros.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(557, 355);
            this.Controls.Add(this.Principal_tableLayoutPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Simulación de Tanque de Cerveza";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Principal_tableLayoutPanel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panelInteriorTanque.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudAlturaTanque)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDiámetro)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel Principal_tableLayoutPanel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panelInteriorTanque;
        private System.Windows.Forms.Panel panelCerveza;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnLlenar;
        private System.Windows.Forms.Button btnVaciar;
        private System.Windows.Forms.Button btnParo;
        private System.Windows.Forms.Panel panelLlenando;
        private System.Windows.Forms.Panel PanelVaciando;
        private System.Windows.Forms.Timer tmrLlenado;
        private System.Windows.Forms.Timer tmrVaciado;
        private System.Windows.Forms.Panel panelVacío;
        private System.Windows.Forms.Panel panelLleno;
        private System.Windows.Forms.Timer tmrEstado;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nudAlturaTanque;
        private System.Windows.Forms.NumericUpDown nudDiámetro;
        private System.Windows.Forms.Label lblNivelCerveza;
        private System.Windows.Forms.Label lblVolumen;
        private System.Windows.Forms.Label lblLitros;
    }
}

