﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulador_de_Tanque_de_Cervecería
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnLlenar_Click(object sender, EventArgs e)
        {
            // Forma de encender o apagar un Timer con la Propiedad ENABLED
            tmrLlenado.Enabled = true;
            tmrVaciado.Enabled = false;
        }

        private void btnVaciar_Click(object sender, EventArgs e)
        {
            // Forma de encender o apagar un Timer con los métodos Start() y Stop()
            tmrVaciado.Start();
            tmrLlenado.Stop();
        }

        private void btnParo_Click(object sender, EventArgs e)
        {
            tmrLlenado.Stop();
            tmrVaciado.Stop();
        }

        private void tmrLlenado_Tick(object sender, EventArgs e)
        {
            if (panelCerveza.Height < panelInteriorTanque.Height)
            {
                panelCerveza.Height++;
            }
            else
            {
                tmrLlenado.Stop();
            }
        }

        private void tmrVaciado_Tick(object sender, EventArgs e)
        {
            if (panelCerveza.Height > 0)
            {
                panelCerveza.Height--;
            }
            else
            {
                tmrVaciado.Stop();
            }
        }

        private void tmrEstado_Tick(object sender, EventArgs e)
        {
            if (tmrLlenado.Enabled)
            {
                panelLlenando.BackColor = Color.Lime;
            }
            else
            {
                panelLlenando.BackColor = Color.FromArgb(0,32,0);
            }

            if (tmrVaciado.Enabled)
            {
                PanelVaciando.BackColor = Color.Lime;
            }
            else
            {
                PanelVaciando.BackColor = Color.FromArgb(0, 32, 0);
            }

            if (panelCerveza.Height == 0)
            {
                panelVacío.BackColor = Color.FromArgb(255, 0, 0);
            }
            else
            {
                panelVacío.BackColor = Color.FromArgb(32, 0, 0);
            }

            if (panelCerveza.Height >= panelInteriorTanque.Height)
            {
                panelLleno.BackColor = Color.Red;
            }
            else
            {
                panelLleno.BackColor = Color.FromArgb(32, 0, 0);
            }

            // Cálculos

            // NIVEL DE CERVEZA
            //  hTanque(m) ---> PanelInteriorTanque.Height
            //  hCerveza(m) --> panelCerveza.Height

            double hCerveza = (double)nudAlturaTanque.Value * 
                panelCerveza.Height / panelInteriorTanque.Height;
            lblNivelCerveza.Text = hCerveza.ToString("0.0000");

            // VOLUMEN DE CERVEZA
            //  V(m³) = Pi * r^2 * h;

            double VolumenM3 = Math.PI * 
                Math.Pow(((double)nudDiámetro.Value / 2), 2) * hCerveza;
            lblVolumen.Text = VolumenM3.ToString("0.0000");

            double Litros = VolumenM3 * 1000;
            lblLitros.Text = Litros.ToString("0.00");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            panelCerveza.Height = 0;
        }
    }
}
